<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_sekolah extends CI_Controller {
	public function index()
	{
		$title['title'] = 'Info Sekolah';
		$data['info_sekolah'] = $this->m_kontak->getInfoSekolah();

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/info_sekolah/info_sekolah', $data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function postEdit($id) {
		$id 			= ['id' => $id];
		$nama_sekolah	= $this->input->post('nama_sekolah');
		$kota 			= $this->input->post('kota');
		$alamat 		= $this->input->post('alamat');
		$telepon 		= $this->input->post('telepon');
		$email_sekolah 	= $this->input->post('email_sekolah');
		$created_by 	= "ADMIN";

		$dataInformasi = [
			'nama_sekolah' 	=> $nama_sekolah,
			'kota' 			=> $kota,
			'alamat' 		=> $alamat,
			'telepon' 		=> $telepon,
			'email_sekolah' => $email_sekolah,
			'created_by' 	=> $created_by
		];
		
		$this->crud->edit($id,$dataInformasi,'tb_m_kontak');
		$this->session->set_flashdata('success', 'Informasi Sekolah berhasil diperbarui!');
		redirect('Info_sekolah/');
		
	}
}