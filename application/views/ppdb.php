<div class="team">
	<div class="contact">

		<!-- Contact Info -->
		<div class="contact_info_container">
			<div class="container">
				<div class="row">

					<!-- Contact Form -->
					<div class="col-lg-6">
						<div class="contact_form">
							<div class="contact_info_title">Pendaftaran PPDB</div>
							<form method="POST" action="<?= base_url('Page/insertPpdb'); ?>" enctype="multipart/form-data">
								<div>
									<div class="form_title">Nama Siswa</div>
									<input type="text" class="comment_input" name="nama_siswa"  value="<?= set_value('nama_siswa'); ?>">
									<small class="form-text text-warning"><?= '<style>small > p{color:red;font-style:italic;}</style>', form_error('nama_siswa'); ?></small>
								</div>
								<div>
									<div class="form_title">Ijazah</div>
									<input type="file" name="ijazah" class="comment_input" accept=".png, .jpg, .jpeg" required="required">
								</div>
								<div>
									<div class="form_title">SKU</div>
									<input type="file" name="sku"  class="comment_input" accept=".png, .jpg, .jpeg" required="required">
								</div>
								<div>
									<div class="form_title">AKK</div>
									<input type="file" name="akk"  class="comment_input" accept=".png, .jpg, .jpeg" required="required">
								</div>
								<div>
									<div class="form_title">KK</div>
									<input type="file" name="kk"  class="comment_input" accept=".png, .jpg, .jpeg" required="required">
								</div>
								<div>
									<div class="form_title">Pernyataan</div>
									<input type="file" name="pernyataan"  class="comment_input" accept=".png, .jpg, .jpeg" required="required">
								</div>

								<div>
									<button type="submit" class="comment_button trans_200">Kirim</button>
								</div>
							</form>
						</div>
					</div>

					<!-- Contact Info -->
					<?php foreach($ppdb as $data) : ?>
					<div class="col-lg-6">
						<div class="contact_info">
							<div class="contact_info_title">Informasi Sekolah</div>
							<div class="contact_info_text">
								<!-- <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a distribution of letters.</p> -->
							</div>
							<div class="contact_info_location">
								<div class="contact_info_location_title"><?= $data->nama_sekolah; ?></div>
								<ul class="location_list">
									<small>Kota</small>
									<li><?= $data->kota; ?></li>
									<small>Alamat</small>
									<li><?= $data->alamat; ?></li>
									<small>No.Telepon</small>
									<li><?= $data->telepon; ?></li>
									<small>Email</small>
									<li><?= $data->email_sekolah; ?></li>
								</ul>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
					
				</div>
			</div>
		</div>
	</div>
</div>