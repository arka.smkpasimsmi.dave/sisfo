
	<!-- Home -->

	<?php foreach($beranda as $data) : ?>
	<div class="home">
		<div class="home_slider_container">
			
			<!-- Home Slider -->
			<div class="owl-carousel owl-theme home_slider">
				
				<!-- Home Slider Item -->
				<div class="owl-item">
					<div class="home_slider_background" style="background-image:url(<?= base_url('assets/images/beranda_images/'.$data->foto_banner); ?>)"></div>
					<div class="home_slider_content" style="top: 35%">
						<div class="container">
							<div class="row">
								<div class="col text-center" data-aos="zoom-out">
									<div class="home_slider_title" style="color: white;"><?= $data->judul_banner ?></div>
									<div class="home_slider_subtitle" style="color: white;"><?= $data->sub_judul_banner ?></div>
									<div class="home_slider_form_container">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		
		<!-- Home Slider Nav -->

	</div>

	<!-- Features -->

		<!-- Latest News -->

		<div class="news" data-aos="fade-up">
			<div class="container">
			<div class="row">
				<div class="col-lg-12 col-sm-12">
					<div class="section_title_container text-center">
						<h2 class="section_title">Sambutan Kepala Sekolah</h2>
						<div class="section_subtitle"><p>Sambutan untuk kepala sekolah SMKN 1</p></div>
					</div>
				</div>
			</div>
			<div class="row news_row">
				<div class="col-lg-6 col-md-12 col-sm-12 news_col">
					
					<!-- News Post Large -->
					<div class="news_post_large_container">
						<div class="news_post_large">
							<div class="news_post_image"><img style="width:100%;max-height: 500px;object-fit: cover;object-position: center;" src="<?= base_url('assets/images/beranda_images/'.$data->foto_kepala_sekolah); ?>" alt="<?= base_url('assets/images/beranda_images/profil_default.png'); ?>"></div>
							<div class="news_post_large_title text-center"><a style="color:#493e3c;">Kepala Sekolah <?= $data->nama_sekolah ?></a></div>
							<div class="news_post_large_title text-center"><a style="color:#493e3c;"><?= $data->nama_kepala_sekolah ?></a></div>
						</div>
					</div>
				</div>

				<div class="col-lg-6 col-md-12 col-sm-12 news_col text-center">
					<div class="news_posts_small">

						<!-- News Posts Small -->
						<div class="news_post_small">
							<div class="news_post_text">
									<p><?= $data->deskripsi_sambutan ?></p>
							</div>
						</div>

					</div>
				</div>
				
					<div class="col-lg-12 mt-5" data-aos="fade-up">
						<div class="section_title_container text-center mt-5">
							<h2 class="section_title">Visi & Misi</h2>
							<div class="section_subtitle"><p>Visi & Misi kami adalah</p></div>
						</div>
					</div>
				<div class="col-lg-6 news_col mt-5" data-aos="fade-up">
					<!-- News Post Large -->
					<div class="news_post_large_container">
						<div class="news_post_large">
							<div class="news_post_large_title text-center"><a style="color:#493e3c;">Visi</a></div>
						</div>
					</div>

					<!-- News Posts Small -->
					<div class="news_post_small news_col" data-aos="fade-up">
							<div class="news_post_text">
									<p><?= $data->deskripsi_visi ?></p>
							</div>
					</div>

				</div>

				<div class="col-lg-6 news_col mt-5" data-aos="fade-up">
					
					<!-- News Post Large -->
					<div class="news_post_large_container" data-aos="fade-up">
						<div class="news_post_large">
							<div class="news_post_large_title text-center"><a style="color:#493e3c;">Misi</a></div>
						</div>
					</div>

					<!-- News Posts Small -->
					<div class="news_post_small news_col" data-aos="fade-up"> 
							<div class="news_post_text">
									<p><?= $data->deskripsi_misi ?></p>
							</div>
					</div>

				</div>

			</div>
		</div>
	</div>
	<?php endforeach; ?>
	<!-- Galeri Kegiatan Sekolah -->

	<div class="courses" data-aos="fade-up">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Galeri Kegiatan Sekolah</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row courses_row">

				<?php foreach($galeri_kegiatan as $data) : ?>
				<!-- List Galeri Kegiatan -->
				<div class="col-lg-4 course_col">
					<div class="course">
							<a href="<?= base_url('assets/images/galeri_images/'.$data->item); ?>" class="single-popup-photo">
                                <div class="course_image"><img style="width:100%;max-height: 252px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/galeri_images/'.$data->item); ?>" ></div>
                            </a>
						<div class="course_body">
							<h3 class="course_title"><a href="<?= base_url('Page/foto'); ?>"><?= $data->judul; ?></a></h3>
							<div class="course_teacher"></div>
							<div class="course_text">
								<p style="width:100%;height:55px;overflow:hidden;text-overflow:ellipsis;"><?= $data->deskripsi; ?></p>
							</div>
						</div>
						<div class="course_footer">
						</div>
					</div>
				</div>
				<?php endforeach; ?>
					
			</div>
			<div class="row">
				<div class="col">
					<div class="courses_button trans_200"><a href="<?= base_url('Page/foto'); ?>">Lihat Semua</a></div>
				</div>
			</div>
		</div>
	</div>

	<!-- End Galeri Kegiatan Sekolah -->


	<!-- Prestasi -->

	<div class="events" data-aos="fade-up">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Prestasi</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row events_row">

				<?php foreach ($prestasi as $data) : ?>
					<div class="col-lg-4 event_col">
					<?php $id = $data->id; ?>
						<div class="event event_left">
							<a href="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" class="single-popup-photo">
                                <div class="event_image"><img style="width:100%;max-height: 252px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" ></div>
                            </a>
							<div class="event_body d-flex flex-row align-items-start justify-content-start">
								<div class="event_date">
									<div class="d-flex flex-column align-items-center justify-content-center trans_200">
										<div class="event_day trans_200"><?= date('d', strtotime($data->created_dt)); ?></div>
										<div class="event_month trans_200"><?= date('M', strtotime($data->created_dt)); ?></div>
									</div>
								</div>
								<div class="event_content">
									<div class="event_title"><a id="judul-prestasi" href="#"><?= $data->nama_prestasi; ?></a></div>
									<div class="event_info_container">
										<div class="event_info"><i class="fa fa-clock-o" aria-hidden="true"></i><span><?= date('d-m-Y', strtotime($data->created_dt)); ?></span></div>
										<div class="event_text">
											<p style="width:100%;height:55px;overflow:hidden;text-overflow:ellipsis;"><?= $data->deskripsi; ?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

				
			</div>
	<!-- End Prestasi -->

				<!-- Lihat Semua -->
				<div class="row">
				<div class="col">
					<div class="courses_button trans_200"><a href="<?= base_url('Page/prestasi'); ?>">Lihat Semua</a></div>
				</div>
			</div>
			
		</div>
	</div>

	<!-- Profil Guru -->

	<div class="team" data-aos="fade-up">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<h2 class="section_title">Profil Guru</h2>
						<div class="section_subtitle"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel gravida arcu. Vestibulum feugiat, sapien ultrices fermentum congue, quam velit venenatis sem</p></div>
					</div>
				</div>
			</div>
			<div class="row team_row">
				
				<!-- List Guru -->
				<?php foreach ($guru as $data) : ?>
					<div class="col-lg-3 col-md-6 team_col">
						<div class="team_item">
							<div class="team_image"><img src="<?= base_url('assets/images/guru_images/'.$data->foto); ?>" alt=""></div>
							<div class="team_body">
								<div class="team_title"><a href="#"><?= $data->nama_guru; ?></a></div>
								<div class="team_subtitle"><?= $data->mapel; ?></div>
								<div class="social_list">
									<ul>
										<li><a href="<?= $data->facebook; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="<?= $data->instagram; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
										<li><a href="mail.com:<?= $data->gmail; ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>

				
			</div>

				<!-- Lihat Semua -->
				<div class="row">
				<div class="col">
					<div class="courses_button trans_200"><a href="<?= base_url('Page/guru'); ?>">Lihat Semua</a></div>
				</div>
			</div>

		</div>
	</div>


	<!-- Newsletter -->

	<div class="newsletter">
		<div class="newsletter_background parallax-window" data-parallax="scroll" data-image-src="images/newsletter.jpg" data-speed="0.8"></div>
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-center justify-content-start">

						<!-- Newsletter Content -->
						<div class="newsletter_content text-lg-left text-center">
							<div class="newsletter_title">sign up for news and offers</div>
							<div class="newsletter_subtitle">Subcribe to lastest smartphones news & great deals we offer</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	        	<img id="popup-img" src="" alt="" class="img-fluid">
	      </div>
	    </div>
	  </div>
	</div>
	
