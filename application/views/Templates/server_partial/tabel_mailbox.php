
<!-- /.col -->
<div class="col-md-9">
  <div class="card card-primary card-outline">
    <div class="card-header">
<form method="POST" action="<?= base_url('Feedback/deletePesan') ?>" id="form-delete" >
      <?php foreach ($pesan as $d): ?>
          <input type="hidden" name="id[]" value="<?= $d->id ?>" hidden>
      <?php endforeach ?>
</form>
      <?php if ($this->uri->segment(2) == 'listPesan'): ?>
        <h3 class="card-title">Pesan Masuk</h3>
        <?php elseif($this->uri->segment(2) == 'terkirimPesan'): ?>
            <h3 class="card-title">Pesan Terkirim</h3>        
        <?php elseif($this->uri->segment(2) == 'pentingPesan'): ?>
            <h3 class="card-title">Pesan Penting</h3>
        <?php elseif($this->uri->segment(2) == 'sampahPesan'): ?>
            <h3 class="card-title">Sampah</h3>
      <?php endif ?>
      <!-- /.card-tools -->
    </div>
    
    <!-- /.card-header -->
    <div class="card-body p-0">
        <div class="card-header row" style="margin-top: -20px;" >
          <?php if ($this->uri->segment(2) == 'sampahPesan'): ?>
                <div class="col-sm-8 offset-sm-3"></div>
            <div class="col col-sm-1">         
                <button type="button" id="btn-delete-enable" class="btn btn-icon btn-danger mt-lg-3"><i class="ik ik-trash"></i></button>
            </div>
        </div>
          <?php endif ?>
      <div class="table-responsive mailbox-messages">
        <table class="table table-hover">
          <?php 
          if ($this->uri->segment(2) == 'listPesan') {
                $this->load->view('server/feedback/list_pesan');
            }elseif ($this->uri->segment(2) == 'terkirimPesan') {
              $this->load->view('server/feedback/terkirim_pesan');
            }elseif ($this->uri->segment(2) == 'pentingPesan') {
              $this->load->view('server/feedback/penting_pesan');
            }elseif ($this->uri->segment(2) == 'sampahPesan') {
              $this->load->view('server/feedback/sampah_pesan');
            }
          ?>
        </table>
        <!-- /.table -->
      </div>
      <!-- /.mail-box-messages -->
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.col -->
</div>
</div>