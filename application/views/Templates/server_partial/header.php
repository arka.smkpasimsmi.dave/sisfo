<div class="wrapper">
            <header class="header-top" header-theme="light">
                <div class="container-fluid">
                    <div class="d-flex justify-content-between">
                        <div class="top-menu d-flex align-items-center">
                            <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                            <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
                        </div>
                        <?php
                            $id = $this->session->userdata('id'); 
                            $data = $this->db->get_where('tb_m_guru', ['id' => $id])->row_array();
                         ?>
                        <div class="top-menu d-flex align-items-center">
                            <div class="dropdown">
                                <?php if ($this->session->userdata('role') == 'admin'): ?>
                                    <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="avatar" src="<?= base_url('assets/images/guru_images/profil_default.png') ?>" alt=""></a>
                                    <?php elseif($this->session->userdata('role') == 'guru'): ?>
                                    <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="avatar" src="<?= base_url('assets/images/guru_images/'.$data['foto']) ?>" alt=""></a>
                                <?php endif ?>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                    <?php if ($this->session->userdata('role') == 'admin'): ?>
                                    <a class="dropdown-item" href="<?= base_url('Admin/settingAdmin') ?>"><i class="ik ik-settings dropdown-icon"></i> Settings</a>
                                    <?php endif ?>
                                    <a class="dropdown-item" href="<?= base_url('Page/logout') ?>"><i class="ik ik-power dropdown-icon"></i> Logout</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </header>
            <div class="page-wrap">